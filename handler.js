'use strict';
const https = require('https');
const AWS = require('aws-sdk');
const iot = new AWS.Iot();

const lambda = new AWS.Lambda({  
	region: "us-east-1"
  });

module.exports.run = (event, context, callback) => {
	context.callbackWaitsForEmptyEventLoop = false;

	void async function(){
		// pega info sobre o hashrate e os works
		let data = await getPoolData();

		// pega valor aproximado em USD e XMR a partir do hashrate
		const earnings = await getEarnings(data.total);

		// sortea um vencedor e em seguida pega quem é o seu refer
		const ganhador = await sorteador(data.total, data.workers);
		let refer = await getRefer(ganhador);
		refer = JSON.parse(refer.Payload).rows[0].refer;

		// verifica se o refer é uma instituição
		const isInstitution = await checkReferIsInstitution(refer);

		// preenche `data` com os valores, vencedores e aplica as margens
		data = transformData(data, earnings, ganhador, refer);
		//console.log("transformed data: ", data);

		// registra RDS com informação do dia completo
		const totalwork = await doRegisterTotalWork(data);
		if (JSON.parse(totalwork.Payload).name === "error"){
			console.error("Erro fatal! Total work: ", totalwork);
			callback(null, totalwork);
			return;
		}

		if (isInstitution){
			const reginstitution = await doRegisterInstitutionPayment(data);
			if (JSON.parse(reginstitution.Payload).name === "error"){
				console.error("Erro fatal! Register institution pay: ", reginstitution);
				callback(null, reginstitution);
				return;
			}
		}

		// registra RDS ordem de pagamento dos vencedores
		const regpayment = await doRegisterPayments(data, isInstitution);
		if (JSON.parse(regpayment.Payload).name === "error"){
			console.error("Erro fatal! Register payment: ", regpayment);
			callback(null, regpayment);
			return;
		}

		// registra no shadow as notificações dos vencedores
		await doRegisterPrizeOnShadow(data.total_work.date, ganhador, data.total_work.usd_winner_prize);
		await doRegisterPrizeOnShadow(data.total_work.date, refer, data.total_work.usd_refer_prize, ganhador);
	}();
}

let getContent = function(url) {
	// return new pending promise
	return new Promise((resolve, reject) => {
	  // select http or https module, depending on reqested url
	  const lib = url.startsWith('https') ? require('https') : require('http');
	  const request = lib.get(url, (response) => {
		// handle http errors
		if (response.statusCode < 200 || response.statusCode > 299) {
		   reject(new Error('Failed to load page, status code: ' + response.statusCode));
		 }
		// temporary data holder
		const body = [];
		// on every content chunk, push it to the data array
		response.on('data', (chunk) => body.push(chunk));
		// we are done, resolve promise with those joined chunks
		response.on('end', () => resolve(body.join('')));
	  });
	  // handle connection errors of the request
	  request.on('error', (err) => reject(err))
	  })
  };  

let getPoolData = async function (){
	const url = 'https://api.nanopool.org/v1/xmr/user/42GkdCcQCxLfb3UupxQ3C8FuVQZTJAMf6iz84PPR9dbT9CRS6tW8PWnfW7kth7LHzM95uSNrP7pMKZCtF2Hr4mxQHFiACQY';
	let request_failed = true;
	let data;
	do {
		try {
			data = await getContent(url);
			request_failed = false;
		} catch (e){
			// try again until we get valid content
		}
	} while (request_failed);

	data = JSON.parse(data);

	data.workers = data.data;

	return data;
}

let getEarnings = async function (total){
	const url = 'https://api.nanopool.org/v1/xmr/approximated_earnings/' + total;
	let data;
	let request_failed = true;
	do {
		try {
			data = await getContent(url);
			request_failed = false;
		} catch (e) {
			// do nothing try again
		}
	} while(request_failed);
	data = JSON.parse(data);

	return {dollars: data.data.day.dollars, coins: data.data.day.coins};
}

let transformData = function(data, earnings, ganhador, refer) {
	const dollars = earnings.dollars;
	const coins = earnings.coins;
	const date = new Date().toJSON().slice(0,10).replace(/-/g,'/');

	data.ganhador = ganhador;
	data.refer = refer;
	data.total_work = {
		"date": date,
		"h24": data.total,
		"number_workers": data.count + "",
		"average_worker_h24": data.total/data.count + "",
		"crypto": 'XMR',
		"crypto_usd": dollars/coins + "",
		"crypto_total": coins + "",
		"usd_total": dollars + "",
		"crypto_winner_prize": coins * 0.45 + "",
		"usd_winner_prize": dollars * 0.45 + "",
		"crypto_refer_prize": coins * 0.05 + "",
		"usd_refer_prize": dollars * 0.05 + "",
		"crypto_donation_prize": coins * 0.10 + "",
		"usd_donation_prize": dollars * 0.10 + "",
		"crypto_company_prize": coins * 0.15 + "",
		"usd_company_prize": dollars * 0.15 + "",
		"crypto_profit_prize": coins * 0.25 + "",
		"usd_profit_prize": dollars * 0.25 + ""
	}

	return data;
}

let callDatabase = async function (query){
	const rds_param = {
		FunctionName: 'db-handler-dev-db_handler',
		Payload: JSON.stringify(query)
	}

	let res;
	try {
		res = await lambda.invoke(rds_param).promise();
		return res;
	} catch (e){
		console.error("Erro fatal! Call database: ", query, e);
		return e;
	}
}

let getRefer = async function(ganhador){
	const query = {
		type: "select",
		table: "things",
		columns: ["refer"],
		where: [{"column": "thing_name", "value": ganhador, "logical": "AND"}]
	}

	return callDatabase(query);
}

let checkReferIsInstitution = async function(refer){
	const url = 'https://s3.amazonaws.com/lucksapp-public/institutions.json';
	let request_failed = true;
	let data;
	do {
		try {
			data = await getContent(url);
			request_failed = false;
		} catch (e){
			// try again until we get valid content
		}
	} while (request_failed);

	data = JSON.parse(data);

	const isInstitution = data.some((v)=>{
		if (v.name === refer){
			return true;
		}
	});

	return isInstitution;
}

let doRegisterTotalWork = async function(data){
	let query = {
		type: "insert",
		table: "total_work"
	}
	query.values = data.total_work;

	return callDatabase(query);
}

let doRegisterInstitutionPayment = async function(data){
	let query = {
		type: "insert",
		table: "institution_prize",
		values: [
			{
				date: data.total_work.date,
				institution: data.refer,
				donator: data.ganhador,
				value: data.total_work.usd_refer_prize
			}
		]
	}

	return callDatabase(query);
}

let doRegisterPayments = async function(data){
	let query = {
		type: "insert",
		table: "payments",
		values: [
			{
				date: data.total_work.date,
				name: data.ganhador,
				claim_date: "",
				value: data.total_work.usd_winner_prize
			}
		]
	}

	const refer_order = {
		date: data.total_work.date,
		name: data.refer,
		claim_date: "",
		value: data.total_work.usd_refer_prize
	}

	if (!isInstitution){
		query.values.push(refer_order);
	}

	return callDatabase(query);
}

let getEndpoint = async function(){
	// pegar endpoint	
	let endpoint;
	try {
		endpoint = await iot.describeEndpoint({}).promise();
		endpoint = endpoint.endpointAddress;
	}
	catch (e){
		console.error("Erro fatal! Describe endpoint: ", e);
		return e;
	}
	return endpoint;
}

let getShadow = async function(name, iotdata){	
	
	var shadow;
	try {
		shadow = await iotdata.getThingShadow({thingName: name}).promise();
		shadow = JSON.parse(shadow.payload);
	}
	catch (e){
		console.error("Erro fatal! Get shadow: ", name, e);
		return e;
	}

	return shadow;
}

let updateShadow = async function(shadow, name, iotdata){

	let update;
	try {
		update = await iotdata.updateThingShadow({thingName: name, payload: Buffer.from(JSON.stringify(shadow))}).promise();
	}
	catch (e){
		console.error("Erro fatal! Update shadow: ", name, e);
		return e;
	}
	return update;
}

let doRegisterPrizeOnShadow = async function(date, name, value, winner){
	const endpoint = await getEndpoint();
	if (!endpoint.constructor === String){
		return endpoint;
	}

	let iotdata = new AWS.IotData({endpoint: endpoint});
	
	let shadow = await getShadow(name, iotdata);
	if (!shadow.state.constructor === Object){
		return shadow;
	}

	shadow = buildUpdateShadow(shadow, winner, value, date);
	
	await updateShadow(shadow, name, iotdata);

	return true;
}

let buildUpdateShadow = function(shadow, refer, value, date){
	shadow.state.desired.prizes.push({refer: refer, value: value, date: date});
	delete shadow.state.delta;

	return shadow;
}

let sorteador = async function (total, workers) {
	
	// valor total de hashrate
	total = total * 10;
	
	// rº hash vencedor
	const r =  Math.floor((Math.random() * total) + 1);
	
	// var para somar os hashs até o r-ésimo
	let atual = 0;
	
	let vencedor = '';
	const date = new Date().toJSON().slice(0,10).replace(/-/g,'/');
	
	let query = {
		type: "insert",
		table: "things_works",
		values: []
	}
					
	// percorrer array buscando o r-ésimo hash	
	// preenche o banco de dados dos workers
	workers.some((e) => {		
		
		// insere o h24 de cada worker no banco			
		query.values.push({date: date, h24: e.h24, thing_name: e.id});
		
		// verifica se o worker atual representa o r-ésimo hash (hash sorteado)
		if (vencedor == ''){
			atual = atual + (e.h24 * 10);
			if (atual >= r){
				vencedor = e.id;
				return true;
			}
		}
	});

	await callDatabase(query);
	
	return vencedor;
}